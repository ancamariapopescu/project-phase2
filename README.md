INSTRUCTIUNI DE RULARE:
1. nvm install 10.13.0
2. node -v (rezultat: v10.13.0)
3. npm -v (rezultat: 6.4.1)
4. mysql-ctl cli install (rezultat: root user si database name)
5. mysql-ctl cli
6. show databases;
7. create database gpsdb;
8. ctrl + C (pt a iesit din mysql)
9. npm init
10. npm install express --save;
11. npm install body-parser --save;
12. npm install sequelize --save;
13. npm install mysql2 --save;
14. npm install nodemon --save-dev;
15. ./node_modules/nodemon/bin/nodemon.js server.js; (pt a porni serverul)

TEST (cu Postman):
1. metoda: POST; URL: "https://proiect-faza2-ancamariapopescu.c9users.io/register-user";
 JSON:
{
	"email":"email@email.com",
	"first_name":"first",
	"last_name":"last",
	"password":"asdfghjklx",
	"user_type":"student",
	"company":"",
	"phone_number":"0740068143"
}
 Raspunsul este:
{
    "id": 1,                  
    "email": "email@email.com",
    "first_name": "first",
    "last_name": "last",
    "password": "asdfghjklx",
    "user_type": "student",
    "company": "",
    "phone_number": "0740068143"
}
A fost creat un user.

2. metoda: POST, URL:"https://proiect-faza2-ancamariapopescu.c9users.io/register-field";
JSON:
{
	"name":"firstfield"
}
 Raspunsul este:
{
    "id": 1,
    "name": "firstfield"
}
A fost creat un domeniu.

3.metoda: POST, URL:"https://proiect-faza2-ancamariapopescu.c9users.io/users/1/fields";
JSON:
{
	"name":"firstfieldforuser1"
}

Raspuns:
{
    "message": "created"
}
 Pentru userul cu id = 1 s-a adaugat un domeniu.
 
 metoda: GET, URL:"https://proiect-faza2-ancamariapopescu.c9users.io/users/1/fields";
 Raspuns:
 [
    {
        "id": 2,
        "name": "firstfieldforuser1",
        "field_items": {
            "user_id": 1,
            "field_id": 2
        }
    }
]